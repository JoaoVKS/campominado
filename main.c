#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#define linhas 20
#define colunas 20
#define bombas 100

 void mostraTab(int tab[linhas][colunas])
 {
 	int i,j;
 	system("cls");
 	printf("__|");
 	for(i=0;i<colunas;i++)
 	{
 		printf("|%2d|", i);	
	}
	printf("\n");
 	for(i=0;i<linhas;i++)
	 {
	 	/*LINHAS*/
	 	printf("%2d|", i);
	 	for(j=0;j<colunas;j++)
		 {
		 	/*COLUNAS*/		 
		 	switch(tab[i][j])
			 {
			 	case -1: printf("[##]"); break;
			 	case 0: printf("[  ]");break;
			 	default: printf("[%2d]", tab[i][j]);
			 }
		 }
		 printf("\n");
	 }
	 
 }
 
 void populaBomba(int tab[linhas][colunas])
 {
 		/*BOMBAS*/
 	int i = 0;
 	int x = 0;
	int y = 0;
	while(i<bombas)
	{
		x= rand()%linhas;
		y= rand()%colunas;
		if(tab[x][y] == 0)
		{
			tab[x][y] = -1;
			i++;
		}
	}
 }

 void contaBomba(int tab[linhas][colunas])
 {
 	/*CONTADOR*/
 	int i = 0;
 	int x = 0;
	int y = 0;
	int cont = 0;
	for(x=0;x<linhas;x++)
	 {
	 	/*LINHAS*/
	 	
	 	for(y=0;y<colunas;y++)
		 {	 
		 	if(tab[x][y] == -1)
		 		continue;
			 
		 	/*cima*/
		 	if(x > 0)
			{
				if(tab[x-1][y] == -1)
				cont++;
			}
			/*baixo*/
			if(x < (linhas-1))
			{
				if(tab[x+1][y] == -1)
				cont++;
			}
			/*direita*/
			if(y < (colunas-1))
			{
				if(tab[x][y+1] == -1)
				cont++;
			}
			/*esquerda*/
			if(y > 0)
			{
				if(tab[x][y-1] == -1)
				cont++;
			}
			/*DcimaDireita*/
			if(x > 0 && y < (colunas-1))
			{
				if(tab[x-1][y+1] == -1)
				cont++;
			}
			/*DcimaEsquerda*/
			if(x > 0 && y > 0)
			{
				if(tab[x-1][y-1] == -1)
				cont++;
			}
			/*DbaixoDireita*/
			if(x < (linhas-1) && y < (colunas-1))
			{
				if(tab[x+1][y+1] == -1)
				cont++;
			}
			/*DbaixoEsquerda*/
			if(x < (linhas-1) && y > 0)
			{
				if(tab[x+1][y-1] == -1)
				cont++;
			}
			
			tab[x][y] = cont;
			cont =0;
		 }
		
	 }
	
 }
int main(int argc, char *argv[]) {
	
	srand(time(NULL));
	
	int i,j;
	int tabuleiro[linhas][colunas];
	int controle[linhas][colunas];
	
	for(i=0;i<linhas;i++)
	{
		for(j=0;j<colunas;j++)
		{
			tabuleiro[i][j] = 0;
		}			
	}
	
	populaBomba(tabuleiro);	
	contaBomba(tabuleiro);
	mostraTab(tabuleiro);
	
	system("color 0a");
	return 0;
}
